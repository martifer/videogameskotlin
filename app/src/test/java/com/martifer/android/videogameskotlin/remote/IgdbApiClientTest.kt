package com.martifer.android.videogameskotlin.data.remote

import com.martifer.android.videogameskotlin.common.TestHelper
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.runBlocking
import org.junit.Test


import retrofit2.Retrofit

class IgdbApiClientTest {

    private val retrofit: Retrofit = mock()
    private val igdbApiClient: IgdbApiClient = mock()

    @Test
    fun getGamesTest () = runBlocking {

        val games = listOf(TestHelper.getGame())
        whenever(igdbApiClient.getGames(TestHelper.getGamesBody(TestHelper.getGame().name))).doReturn(games)

        val response = igdbApiClient.getGames(TestHelper.getGamesBody(TestHelper.getGame().name))

        assert(response.isNotEmpty())
    }
}