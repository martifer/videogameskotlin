package com.martifer.android.videogameskotlin.domain.use_cases

import com.martifer.android.videogameskotlin.common.TestHelper
import com.martifer.android.videogameskotlin.data.repository.VideogamesRepositoryApi
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import kotlinx.coroutines.runBlocking
import org.junit.Test

class InsertGameUseCaseTest {

    private val repository: VideogamesRepositoryApi = mock()
    private val useCase = InsertGameUseCase(repository)

    @Test
    fun `should invoke repository and insert game when is invoked`() = runBlocking{
        val gameEntity = TestHelper.getGameEntity()
        useCase.invoke(gameEntity)
        verify(repository).insertGame(gameEntity)
    }
}