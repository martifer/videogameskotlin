package com.martifer.android.videogameskotlin.domain.use_cases

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import com.martifer.android.videogameskotlin.common.TestHelper
import com.martifer.android.videogameskotlin.data.repository.VideogamesRepositoryApi
import kotlinx.coroutines.flow.take
import kotlinx.coroutines.flow.toSet
import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.junit.Assert.assertEquals
import retrofit2.HttpException

class GetGamesUseCaseTest {

    private val repository: VideogamesRepositoryApi = mock()
    private val useCase = GetGamesUseCase(repository)

    @Test
    fun `should invoke repository and return a list of games when is invoked`() = runBlocking{

        val games = listOf(TestHelper.getGame())
        whenever(repository.getGames(games.get(0).name)).thenReturn(games)
        val outputFlow = useCase.invoke(games.get(0).name)

        val list = outputFlow.take(2).toSet()

        val last = list.last()  // first element is just "emit loading", so we only need the second element

        assertEquals(last.data,games)
    }

    @Test
    fun `should invoke repository and return flow with loading resource as first element`() = runBlocking{

        val games = listOf(TestHelper.getGame())
        whenever(repository.getGames(games.get(0).name)).thenReturn(games)
        val outputFlow = useCase.invoke(games.get(0).name)

        val list = outputFlow.take(2).toSet()

        val first = list.first()  // first element is just "emit loading"

        assertEquals(first.data,null)
    }
    @Test
    fun `should invoke repository and return unexpected error`() = runBlocking{

        val games = listOf(TestHelper.getGame())
        val httpException: HttpException = mock()
        whenever(repository.getGames(games.get(0).name)).thenThrow(httpException)

        val outputFlow = useCase.invoke(games.get(0).name)

        val list = outputFlow.toSet()

        val last = list.last()  // last element should be an exeption

        assertEquals(last.data,null)
        assertEquals(last.message,"An unexpected error occured")
    }
}