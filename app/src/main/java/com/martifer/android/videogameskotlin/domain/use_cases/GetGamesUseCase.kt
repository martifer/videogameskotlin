package com.martifer.android.videogameskotlin.domain.use_cases

import com.martifer.android.videogameskotlin.common.Resource
import com.martifer.android.videogameskotlin.data.Game
import com.martifer.android.videogameskotlin.data.repository.VideogamesRepositoryApi
import com.martifer.android.videogameskotlin.domain.model.response.ResponseGameModel
import kotlinx.coroutines.flow.Flow

import kotlinx.coroutines.flow.flow
import retrofit2.HttpException
import java.io.IOException

class GetGamesUseCase (private val repositoryImpl: VideogamesRepositoryApi) {
    operator fun invoke(name:String): Flow<Resource<List<ResponseGameModel>>> = flow{
        try {
            emit(Resource.Loading())
            val games = repositoryImpl.getGames(name)
            emit(Resource.Success(games))
        } catch (e: HttpException) {
            emit(Resource.Error<List<ResponseGameModel>>(e.localizedMessage ?: "An unexpected error occured"))
        } catch(e: IOException) {
            emit(Resource.Error<List<ResponseGameModel>>("Couldn't reach server. Check your internet connection."))
        }
    }
}
