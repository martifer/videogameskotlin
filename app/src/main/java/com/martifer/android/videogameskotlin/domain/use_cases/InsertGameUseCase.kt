package com.martifer.android.videogameskotlin.domain.use_cases

import com.martifer.android.videogameskotlin.data.GameEntity
import com.martifer.android.videogameskotlin.data.repository.VideogamesRepositoryApi

class InsertGameUseCase (private val repositoryImpl: VideogamesRepositoryApi) {
    suspend operator fun invoke(gameEntity: GameEntity) = repositoryImpl.insertGame(gameEntity)
}