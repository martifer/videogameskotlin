package com.martifer.android.videogameskotlin.common

import com.martifer.android.videogameskotlin.data.Game
import com.martifer.android.videogameskotlin.data.GameEntity
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody

class TestHelper {
    companion object {
        fun getGame() =
            Game(
                id = 0,
                name = "Zelda",
                game = 9,
                firstReleaseDate = "2",
                summary = "hola",
                rating = 5.6f
            )

        fun getGameEntity(): GameEntity {
            return GameEntity(getGame())
        }

        fun getGamesBody(name: String): RequestBody {
            val body =
                "fields *; search \"" + name + "\"; limit 50;"
            return body.toRequestBody("application/json".toMediaTypeOrNull())
        }
    }
}