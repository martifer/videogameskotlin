package com.martifer.android.videogameskotlin.di

import android.app.Application
import androidx.room.Room
import com.martifer.android.videogameskotlin.data.repository.VideogamesRepositoryApi
import com.martifer.android.videogameskotlin.data.local.ConsultaDao
import com.martifer.android.videogameskotlin.data.local.ConsultaDatabase
import com.martifer.android.videogameskotlin.data.local.GameDao
import com.martifer.android.videogameskotlin.data.local.GameDatabase
import com.martifer.android.videogameskotlin.data.remote.IgdbApiClient
import com.martifer.android.videogameskotlin.domain.use_cases.*
import com.martifer.android.videogameskotlin.ui.gamedetail.GameDetailFragment
import com.martifer.android.videogameskotlin.ui.gamedetail.GameDetailViewModel
import com.martifer.android.videogameskotlin.ui.statistics.StatisticsFragment
import com.martifer.android.videogameskotlin.ui.statistics.StatisticsViewModel
import com.martifer.android.videogameskotlin.ui.favorites.FavoritesViewModel
import com.martifer.android.videogameskotlin.ui.main.MainFragment
import com.martifer.android.videogameskotlin.ui.main.MainViewModel
import kotlinx.coroutines.Dispatchers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidApplication
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val repositoryModule = module {
    single { VideogamesRepositoryApi(get(), get(), get()) }
}

val remoteModule = module {
    single { provideRetrofit(get()) }
    single { provideIgdbApiClient(get()) }
    factory { provideLoggingInterceptor() }
    factory { provideOkHttpClient(get()) }
}

fun provideIgdbApiClient(retrofit: Retrofit): IgdbApiClient = retrofit.create(IgdbApiClient::class.java)

fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit {
    return Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl("https://api.igdb.com/v4/")
            .client(okHttpClient)
            .build()
}

fun provideOkHttpClient( loggingInterceptor: HttpLoggingInterceptor): OkHttpClient {
    return OkHttpClient.Builder().apply {
        this.addInterceptor(loggingInterceptor)
    }.build()
}

fun provideLoggingInterceptor(): HttpLoggingInterceptor {
    return HttpLoggingInterceptor().apply {
        this.level = HttpLoggingInterceptor.Level.BODY
    }
}

val localModule = module {

    single { provideConsultaDatabase(androidApplication()) }
    single { provideConsultaDao(get()) }
    single { provideGameDatabase(androidApplication()) }
    single { provideGameDao(get()) }
}

fun provideConsultaDatabase(application: Application): ConsultaDatabase {
    return Room.databaseBuilder(
            application,
            ConsultaDatabase::class.java,
            "consulta_database"
    )
            //.addCallback(ConsultaDatabaseCallback(scope))
            .build()
}

fun provideConsultaDao(database: ConsultaDatabase): ConsultaDao {
    return  database.consultaDao()
}

fun provideGameDatabase(application: Application): GameDatabase {
    return Room.databaseBuilder(
        application,
        GameDatabase::class.java,
        "game_database"
    )
        .build()
}

fun provideGameDao(database: GameDatabase): GameDao {
    return  database.gameDao()
}


val mainViewModelModule = module {
    viewModel {
        MainViewModel(get(),get(), get(), get())
    }
}

val statisticsViewModelModule = module {
    viewModel {
        StatisticsViewModel(get())
    }
}

val gameDetailViewModelModule = module {
    viewModel {
        GameDetailViewModel(get(),get())
    }
}

val favoritesViewModelModule = module {
    viewModel {
        FavoritesViewModel(get(),get ())
    }
}

val mainFragmentModule = module {
    factory { MainFragment() }
}

val gameDetailFragmentModule = module {
    factory { GameDetailFragment() }
}

val statisticsFragmentModule = module {
    factory { StatisticsFragment() }
}

val useCasesModule = module {
    factory { provideGetGameUseCase(get()) }
    factory { provideGetGamesUseCase(get()) }
    factory { provideInsertGameUseCase(get()) }
    factory { provideGetImageUseCase(get()) }
    factory { provideGetStoredGamesUseCase(get()) }
}


fun provideGetGameUseCase(repositoryImpl: VideogamesRepositoryApi): GetGameUseCase = GetGameUseCase(repositoryImpl)
fun provideGetGamesUseCase(repositoryImpl: VideogamesRepositoryApi): GetGamesUseCase = GetGamesUseCase(repositoryImpl)
fun provideInsertGameUseCase(repositoryImpl: VideogamesRepositoryApi): InsertGameUseCase = InsertGameUseCase(repositoryImpl)
fun provideGetImageUseCase(repositoryImpl: VideogamesRepositoryApi): GetImageUseCase = GetImageUseCase(repositoryImpl)
fun provideGetStoredGamesUseCase(repositoryImpl: VideogamesRepositoryApi): GetStoredGamesUseCase = GetStoredGamesUseCase(repositoryImpl)

val dispatchersModule = module {
    factory { provideDispatchers() }

}

fun provideDispatchers(): Dispatchers = Dispatchers