package com.martifer.android.videogameskotlin.ui.main

import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.SearchView
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.martifer.android.videogameskotlin.R
import com.martifer.android.videogameskotlin.data.Game
import kotlinx.android.synthetic.main.main_fragment.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainFragment : Fragment() {

    private val viewModel: MainViewModel by viewModel()

    private lateinit var vista: View

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.v("MainFragment", "onCreate");
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.v("MainFragment", "onViewCreated");
        vista = view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        Log.v("MainFragment", "onActivityCreated");
        super.onActivityCreated(savedInstanceState)
        attachObserver()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        Log.d("MainFragment", "onCreateOptionsMenu");
        inflater.inflate(R.menu.main_menu, menu)

        val search = menu.findItem(R.id.search)
        val searchView = search.actionView as SearchView
        searchView.queryHint = "Busca"
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {

                searchView.clearFocus()
                viewModel.getGames(query!!)
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                return true
            }
        })
        return super.onCreateOptionsMenu(menu, inflater)
    }

    private fun goToDetails(id: String){
        val action = MainFragmentDirections.actionGoToDetails(id)
        vista.findNavController().navigate(action)
    }

    private fun attachObserver() {
        Log.v("MainFragment", "attachObserver");
        viewModel.responseLiveData.observe(viewLifecycleOwner, Observer<List<Game>> {
            it?.let { setupRecycler(it) }
        })
        viewModel.isLoading.observe(viewLifecycleOwner){
            progressMain.isVisible = it
        }
    }

    fun setupRecycler(gameList: List<Game>) {
        Log.v("MainFragment", "setupRecycler");
        games_recycler.setHasFixedSize(true)
        val layoutManager = LinearLayoutManager(this.context)
        layoutManager.orientation = LinearLayoutManager.VERTICAL
        games_recycler.layoutManager = layoutManager

        val listener1: (Game) -> Unit = {goToDetails(it.game.toString())}
        val listener2: (Game) -> Unit = {viewModel.addToFavorite(it.game.toString())
             }

        games_recycler.adapter = MainAdapter(gameList,listener1, listener2) /*{
            Log.v("Game: ", it.name)
            goToDetails(it.game.toString())
        }*/
    }
}
