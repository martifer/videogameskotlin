package com.martifer.android.videogameskotlin.data

data class Game(
    val name: String,
    val game: Int,
    val id: Int,
    val firstReleaseDate: String,
    val rating: Float,
    val summary: String
)