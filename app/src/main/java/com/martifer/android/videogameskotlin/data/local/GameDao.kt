package com.martifer.android.videogameskotlin.data.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.martifer.android.videogameskotlin.data.GameEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface GameDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(gameEntity: GameEntity)

    @Query("SELECT * FROM game_table")
    fun getStoredGames(): Flow<List<GameEntity>>
}