package com.martifer.android.videogameskotlin.ui.statistics

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.martifer.android.videogameskotlin.R
import androidx.lifecycle.observe
import kotlinx.android.synthetic.main.statistics_fragment.*
import org.koin.androidx.viewmodel.ext.android.viewModel


class StatisticsFragment: Fragment() {

    private val viewModel: StatisticsViewModel by viewModel()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        val view =  inflater.inflate(R.layout.statistics_fragment, container, false)
        return view
    }

    @SuppressLint("FragmentLiveDataObserve")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        pintaConsultes()
    }

    @SuppressLint("FragmentLiveDataObserve")
    private fun pintaConsultes() {
        context
        val adapter = StatisticsListAdapter()
        statistics_recycler.adapter = adapter
        statistics_recycler.layoutManager = LinearLayoutManager(context)

        // Add an observer on the LiveData returned by getAlphabetizedWords.
        // The onChanged() method fires when the observed data changes and the activity is
        // in the foreground.
        viewModel.consultes.observe(owner = this) { consultes ->
            consultes.let { adapter.submitList(it) }
        }
    }
}