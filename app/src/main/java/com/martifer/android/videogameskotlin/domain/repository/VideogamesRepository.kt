package com.martifer.android.videogameskotlin.domain.repository

import com.martifer.android.videogameskotlin.data.Consulta
import com.martifer.android.videogameskotlin.data.Game
import com.martifer.android.videogameskotlin.data.GameEntity
import com.martifer.android.videogameskotlin.data.GameImage
import com.martifer.android.videogameskotlin.domain.model.response.ResponseGameModel
import kotlinx.coroutines.flow.Flow

interface VideogamesRepository {
    suspend fun getGames(name: String): List<ResponseGameModel>
    suspend fun getGame(id: String): List<Game>
    suspend fun getArtworks(gameId: String): List<GameImage>
    suspend fun insert(consulta: Consulta)
    suspend fun insertGame(gameEntity: GameEntity)
    suspend fun deleteAll()
    fun getAlphabetizedConsultes(): Flow<List<Consulta>>
    fun getStoredGames(): Flow<List<GameEntity>>
}