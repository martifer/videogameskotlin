package com.martifer.android.videogameskotlin.data.model.response

import com.martifer.android.videogameskotlin.domain.model.response.ResponseGameModel

fun ResponseGameApi.toDomainModel() = ResponseGameModel(
    name = name,
    game = game,
    id = id,
    firstReleaseDate = firstReleaseDate,
    rating = rating,
    summary = summary
)