package com.martifer.android.videogameskotlin.domain.use_cases

import com.martifer.android.videogameskotlin.common.Resource
import com.martifer.android.videogameskotlin.data.GameEntity
import com.martifer.android.videogameskotlin.data.repository.VideogamesRepositoryApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first

import kotlinx.coroutines.flow.flow

class GetStoredGamesUseCase (private val repositoryImpl: VideogamesRepositoryApi) {
    operator fun invoke(): Flow<Resource<List<GameEntity>>> = flow {
        emit(Resource.Loading())
        val games = repositoryImpl.getStoredGames()
        emit(Resource.Success(games.first()))
    }
}
