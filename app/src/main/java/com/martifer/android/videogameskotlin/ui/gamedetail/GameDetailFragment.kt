package com.martifer.android.videogameskotlin.ui.gamedetail

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.martifer.android.videogameskotlin.R
import com.martifer.android.videogameskotlin.databinding.DetailsFragmentBinding
import org.koin.androidx.viewmodel.ext.android.viewModel


class GameDetailFragment: Fragment() {

    val app by lazy {requireActivity().application}

    private val viewModel: GameDetailViewModel by viewModel()

    private lateinit var viewDataBinding: DetailsFragmentBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        val view =  inflater.inflate(R.layout.details_fragment, container, false)

        viewDataBinding = DetailsFragmentBinding.bind(view).apply {
            viewmodel = viewModel
        }
        viewDataBinding.lifecycleOwner = this.viewLifecycleOwner

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.responseLiveDataGameImage.observe(this.viewLifecycleOwner, Observer {
            viewModel.setImageView(view)
        })
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {

        super.onActivityCreated(savedInstanceState)

        arguments?.let{
            //Using SafeArgs
            val id = GameDetailFragmentArgs.fromBundle(it).idArg
            Log.v("GameDetailsFragment", "arguments from bundle: id: " +  id);
            viewModel.getGame(id)
        }
    }
}