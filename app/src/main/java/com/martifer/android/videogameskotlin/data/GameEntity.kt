package com.martifer.android.videogameskotlin.data

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "game_table")
class GameEntity(
    @Embedded
    var game: Game
) {

    @PrimaryKey(autoGenerate = true)
    var gameId = game.id

}

fun GameEntity.toGame(): Game {
    return Game(
        game = game.game,
        name = game.name,
        id = game.id,
        firstReleaseDate = game.firstReleaseDate,
        rating = game.rating,
        summary = game.summary
    )
}