package com.martifer.android.videogameskotlin.domain.use_cases

import com.martifer.android.videogameskotlin.common.Resource
import com.martifer.android.videogameskotlin.data.GameImage
import com.martifer.android.videogameskotlin.data.repository.VideogamesRepositoryApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.HttpException
import java.io.IOException

class GetImageUseCase (private val repositoryImpl: VideogamesRepositoryApi) {
    operator fun invoke(name:String): Flow<Resource<List<GameImage>>> = flow{
        try {
            emit(Resource.Loading<List<GameImage>>())
            val games = repositoryImpl.getArtworks(name)
            emit(Resource.Success(games))
        } catch (e: HttpException) {
            emit(Resource.Error<List<GameImage>>(e.localizedMessage ?: "An unexpected error occured"))
        } catch(e: IOException) {
            emit(Resource.Error<List<GameImage>>("Couldn't reach server. Check your internet connection."))
        }
    }
}
