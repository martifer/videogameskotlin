package com.martifer.android.videogameskotlin.ui.gamedetail

import android.util.Log
import android.view.View
import android.widget.ImageView
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.martifer.android.videogameskotlin.R
import com.martifer.android.videogameskotlin.common.Resource
import com.martifer.android.videogameskotlin.data.Game
import com.martifer.android.videogameskotlin.data.GameImage
import com.martifer.android.videogameskotlin.domain.use_cases.GetGameUseCase
import com.martifer.android.videogameskotlin.domain.use_cases.GetImageUseCase
import com.squareup.picasso.Picasso
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

class GameDetailViewModel(private val getGameUseCase: GetGameUseCase,
                          private val getImageUseCase: GetImageUseCase,
                          private val dispatcher: CoroutineDispatcher = Dispatchers.Main
                          ) : ViewModel(){

    val responseLiveDataDetail = MutableLiveData<List<Game>>()
    val responseLiveDataGameImage = MutableLiveData<List<GameImage>>()

    fun getGame(name: String) {
        getGameUseCase(name).onEach { result ->
            when (result) {
                is Resource.Success -> {
                    withContext(dispatcher) {
                        responseLiveDataDetail.value = result.data
                        getImage(result.data!![0].id.toString())
                    }
                }
                is Resource.Error -> {
                    Log.d("DetailsVM", "error GetGame" +result.data)
                    onError("Error : ${result.message} ")
                }
                is Resource.Loading -> {
                    //TODO:
                }
            }
        }.launchIn(viewModelScope)
    }

    fun getImage(gameId: String) {

        getImageUseCase(gameId).onEach { result ->
            when (result) {
                is Resource.Success -> {
                    withContext(dispatcher) {
                        responseLiveDataGameImage.value = result.data
                    }
                }
                is Resource.Error -> {
                    Log.d("DetailsVM", "error GetGame" +result.data)
                    onError("Error : ${result.message} ")
                }
                is Resource.Loading -> {
                    //TODO
                }
            }
        }.launchIn(viewModelScope)
    }

    private fun onError(message: String) {
    }

    fun setImageView(view: View) {
        val urlImage = "https:"+responseLiveDataGameImage.value?.get(0)?.url
        val urlImageMed = urlImage.replace("t_thumb","t_screenshot_med_2x")
        val image: ImageView = view.findViewById(R.id.imageDetails)
        Picasso.get().load(urlImageMed).into(image)
    }
}
