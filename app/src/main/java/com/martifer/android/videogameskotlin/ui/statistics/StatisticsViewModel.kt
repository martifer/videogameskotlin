package com.martifer.android.videogameskotlin.ui.statistics

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import com.martifer.android.videogameskotlin.data.Consulta
import com.martifer.android.videogameskotlin.data.repository.VideogamesRepositoryApi

class StatisticsViewModel(private val repositoryImpl: VideogamesRepositoryApi) : ViewModel() {
    //Observed Flow will notify the observer when the data has changed.
    val consultes: LiveData<List<Consulta>> = repositoryImpl.getAlphabetizedConsultes().asLiveData()
}