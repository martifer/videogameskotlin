package com.martifer.android.videogameskotlin.ui.main

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.martifer.android.videogameskotlin.common.Resource
import com.martifer.android.videogameskotlin.data.*
import com.martifer.android.videogameskotlin.data.repository.VideogamesRepositoryApi
import com.martifer.android.videogameskotlin.domain.use_cases.GetGameUseCase
import com.martifer.android.videogameskotlin.domain.use_cases.GetGamesUseCase
import com.martifer.android.videogameskotlin.domain.use_cases.InsertGameUseCase
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

class MainViewModel(private val repositoryImpl: VideogamesRepositoryApi,
                    private val getGameUseCase: GetGameUseCase,
                    private val getGamesUseCase: GetGamesUseCase,
                    private val insertGameUseCase: InsertGameUseCase,
                    private val dispatcher: CoroutineDispatcher = Dispatchers.Main
) : ViewModel() {

    val responseLiveData = MutableLiveData<List<Game>>()
    val isLoading = MutableLiveData<Boolean>()

    fun getGames(name: String) {
        getGamesUseCase(name).onEach { result ->
            when (result) {
                is Resource.Success -> {
                    withContext(dispatcher) {
                        responseLiveData.value = result.data
                        insertaConsulta(name)
                        isLoading.postValue(false)
                    }
                }
                is Resource.Error -> {
                    Log.d("MainVM", "error GetGames" +result.data)
                    onError("Error : ${result.message} ")
                    isLoading.postValue(false)
                }
                is Resource.Loading -> {
                   isLoading.postValue(true)
                }
            }
        }.launchIn(viewModelScope)
    }

    fun addToFavorite(id: String) {
        getGameUseCase(id).onEach { result ->
            when (result) {
                is Resource.Success -> {
                    if(result.data!!.isNotEmpty()) {
                        val game = result.data!![0]
                        insertGame(GameEntity(game))
                    } else {
                        //TODO: no result found
                    }
                }
                is Resource.Error -> {
                    Log.d("DetailsVM", "error GetGame" +result.data)
                    onError("Error : ${result.message} ")
                }
                is Resource.Loading -> {
                    // _state.value = CoinListState(isLoading = true)
                }
            }
        }.launchIn(viewModelScope)
    }

    private fun onError(message: String) {
        Log.v("TAG",message)
    }

    private suspend fun insertaConsulta(name: String) {
        val consulta = Consulta(name,1)
        repositoryImpl.insert(consulta)
    }

    private suspend fun insertGame(gameEntity: GameEntity) {
        insertGameUseCase.invoke(gameEntity)
    }
}