package com.martifer.android.videogameskotlin.domain.use_cases

import com.martifer.android.videogameskotlin.common.Resource
import com.martifer.android.videogameskotlin.data.Game
import com.martifer.android.videogameskotlin.data.repository.VideogamesRepositoryApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.HttpException
import java.io.IOException


class GetGameUseCase (private val repositoryImpl: VideogamesRepositoryApi) {
    operator fun invoke(name: String): Flow<Resource<List<Game>>> = flow {
        try {
            emit(Resource.Loading<List<Game>>())
            val games = repositoryImpl.getGame(name)
            emit(Resource.Success(games))
        } catch (e: HttpException) {
            emit(Resource.Error<List<Game>>(e.localizedMessage ?: "An unexpected error occured"))
        } catch (e: IOException) {
            emit(Resource.Error<List<Game>>("Couldn't reach server. Check your internet connection."))
        }
    }
}