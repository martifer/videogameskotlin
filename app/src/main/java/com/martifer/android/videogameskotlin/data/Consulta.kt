package com.martifer.android.videogameskotlin.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "taula")
class Consulta(@PrimaryKey @ColumnInfo(name = "nom") val nom: String,
                           @ColumnInfo(name = "data") var data_consulta: Int
)