package com.martifer.android.videogameskotlin.data.model.response

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class ResponseGameApi(
    @SerializedName("name") val name: String,
    @SerializedName("game") val game: Int,
    @SerializedName("id") val id: Int,
    @SerializedName("firstReleaseDate") val firstReleaseDate: String,
    @SerializedName("rating") val rating: Float,
    @SerializedName("summary") val summary: String
) : Serializable