package com.martifer.android.videogameskotlin.domain.model.response

import java.io.Serializable

data class ResponseGameModel(
    val name: String,
    val game: Int,
    val id: Int,
    val firstReleaseDate: String,
    val rating: Float,
    val summary: String
): Serializable
