package com.martifer.android.videogameskotlin.ui.favorites

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.martifer.android.videogameskotlin.R
import com.martifer.android.videogameskotlin.data.toGame
import kotlinx.android.synthetic.main.favorites_fragment.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class FavoritesFragment: Fragment()  {

    private val viewModel: FavoritesViewModel by viewModel()

    private lateinit var adapter: FavoritesListAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)

        val view =  inflater.inflate(R.layout.favorites_fragment, container, false)

        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupRecycler()
        attachObserver()
        viewModel.allFavoriteGames
    }

    fun setupRecycler() {

        adapter = FavoritesListAdapter()

        favorites_recycler.adapter = adapter
        favorites_recycler.layoutManager = LinearLayoutManager(context)

    }

    private fun attachObserver() {

        Log.v("MainFragment", "attachObserver");
        viewModel.allFavoriteGames.observe(viewLifecycleOwner) { games ->
            games.let {
                val list = it.data?.map { it.toGame() }
                adapter.submitList(list)
            }
        }
    }
}