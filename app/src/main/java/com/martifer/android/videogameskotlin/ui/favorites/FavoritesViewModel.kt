package com.martifer.android.videogameskotlin.ui.favorites

import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import com.martifer.android.videogameskotlin.data.repository.VideogamesRepositoryApi
import com.martifer.android.videogameskotlin.domain.use_cases.GetStoredGamesUseCase

class FavoritesViewModel(private val getStoredGamesUseCase: GetStoredGamesUseCase,
                         private val repository: VideogamesRepositoryApi
) : ViewModel(){
    val allFavoriteGames = getStoredGamesUseCase.invoke().asLiveData()
}