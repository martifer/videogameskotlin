package com.martifer.android.videogameskotlin.ui.main

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.martifer.android.videogameskotlin.R
import com.martifer.android.videogameskotlin.data.Game
import kotlinx.android.synthetic.main.item_main_layout.view.*

class MainAdapter(
    private val gameList: List<Game>,
    private val listener: (Game) -> Unit,
    private val listener2: (Game) -> Unit
): RecyclerView.Adapter<MainAdapter.GameHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = GameHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.item_main_layout, parent, false))

    override fun onBindViewHolder(holder: GameHolder, position: Int)  {
        holder.bind(gameList[position], listener, listener2)
    }

    override fun getItemCount() = gameList.size

    class GameHolder(view: View): RecyclerView.ViewHolder(view) {

        fun bind(game: Game, listener: (Game) -> Unit, listener2: (Game) -> Unit) = with(itemView) {
            name.text = game.name
            published_at.text = game.firstReleaseDate
            setOnClickListener { listener(game) }
            button_favorite.setOnClickListener {listener2(game)}
        }
    }
}