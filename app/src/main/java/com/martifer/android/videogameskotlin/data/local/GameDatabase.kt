package com.martifer.android.videogameskotlin.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.martifer.android.videogameskotlin.data.GameEntity

@Database(entities = [GameEntity::class], version = 1)
abstract class GameDatabase : RoomDatabase() {

    abstract fun gameDao(): GameDao
}