package com.martifer.android.videogameskotlin.ui.statistics

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.martifer.android.videogameskotlin.R
import com.martifer.android.videogameskotlin.data.Consulta
import kotlinx.android.synthetic.main.consulta_item_layout.view.*

class StatisticsAdapter(
        private val consultaList: List<Consulta>,
        private val listener: (Consulta) -> Unit
): RecyclerView.Adapter<StatisticsAdapter.ConsultaHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ConsultaHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.consulta_item_layout, parent, false))

    override fun onBindViewHolder(holder: ConsultaHolder, position: Int)  {
        holder.bind(consultaList[position], listener)
    }

    override fun getItemCount() = consultaList.size

    class ConsultaHolder(view: View): RecyclerView.ViewHolder(view) {

        fun bind(consulta: Consulta, listener: (Consulta) -> Unit) = with(itemView) {
            nom_consulta.text = consulta.nom
            data_consulta.text = consulta.data_consulta.toString()
            setOnClickListener { listener(consulta) }
        }
    }
}