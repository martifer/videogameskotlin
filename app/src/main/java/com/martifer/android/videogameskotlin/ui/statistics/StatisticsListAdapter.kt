package com.martifer.android.videogameskotlin.ui.statistics

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.martifer.android.videogameskotlin.R
import com.martifer.android.videogameskotlin.data.Consulta
import com.martifer.android.videogameskotlin.ui.statistics.StatisticsListAdapter.ConsultaViewHolder

class StatisticsListAdapter : ListAdapter<Consulta, ConsultaViewHolder>(CONSULTES_COMPARATOR) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ConsultaViewHolder {
        return ConsultaViewHolder.create(parent)
    }

    override fun onBindViewHolder(holder: ConsultaViewHolder, position: Int) {
        val current = getItem(position)
        holder.bind(current.nom,current.data_consulta)
    }

    class ConsultaViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val nomItemView: TextView = itemView.findViewById(R.id.textView)
        private val dataItemView: TextView = itemView.findViewById(R.id.textView2)

        fun bind(text: String?, text2: Int?) {
            nomItemView.text = text
            dataItemView.text = text2.toString()
        }

        companion object {
            fun create(parent: ViewGroup): ConsultaViewHolder {
                val view: View = LayoutInflater.from(parent.context)
                        .inflate(R.layout.item_statistics_layout, parent, false)
                return ConsultaViewHolder(view)
            }
        }
    }

    companion object {
        private val CONSULTES_COMPARATOR = object : DiffUtil.ItemCallback<Consulta>() {
            override fun areItemsTheSame(oldItem: Consulta, newItem: Consulta): Boolean {
                return oldItem === newItem
            }

            override fun areContentsTheSame(oldItem: Consulta, newItem: Consulta): Boolean {
                return oldItem.nom == newItem.nom
            }
        }
    }
}