package com.martifer.android.videogameskotlin.data.local

import android.content.Context
import android.util.Log
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.martifer.android.videogameskotlin.data.Consulta
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

@Database(entities = [Consulta::class], version = 1)
abstract class ConsultaDatabase() : RoomDatabase() {

    abstract fun consultaDao(): ConsultaDao

    companion object {
        private const val DATABASE_NAME = "consulta_database"
        @Volatile
        private var INSTANCE: ConsultaDatabase? = null

        fun getInstance(context: Context, scope: CoroutineScope): ConsultaDatabase? {
            Log.v("consultaDao", "getInstance");
            INSTANCE ?: synchronized(this) {
                INSTANCE = Room.databaseBuilder(
                        context.applicationContext,
                        ConsultaDatabase::class.java,
                        DATABASE_NAME
                ).addCallback(ConsultaDatabaseCallback(scope))
                    .build()
            }
            return INSTANCE
        }
    }

    private class ConsultaDatabaseCallback(
        private val scope: CoroutineScope,
        private val dispatcher: CoroutineDispatcher = Dispatchers.IO
    ) : RoomDatabase.Callback() {
        /**
         * Override the onCreate method to populate the database.
         */
        override fun onCreate(db: SupportSQLiteDatabase) {
            super.onCreate(db)
            Log.v("ConsultDatabaseCallback", "onCreate");

            // If you want to keep the data through app restarts,
            // comment out the following line.
            INSTANCE?.let { database ->
                scope.launch(dispatcher) {
                    populateDatabase(database.consultaDao())
                }
            }
        }

        override fun onOpen(db: SupportSQLiteDatabase) {
            super.onOpen(db)
            INSTANCE?.let {
                scope.launch(dispatcher) {
                    //populateDatabase(database.consultaDao())
                }
            }
        }
        /**
         * Populate the database in a new coroutine.
         */
        suspend fun populateDatabase(consultaDao: ConsultaDao) {
            Log.v("ConsultDatabaseCallback", "populateDatabase");
            // Start the app with a clean database every time.
            // Not needed if you only populate on creation.
            consultaDao.deleteAll()

            var consulta = Consulta("Zelda",1)
            consultaDao.insert(consulta)
            consulta = Consulta("Mario",2)
            consultaDao.insert(consulta)
            consulta = Consulta("Sonic",3)
            consultaDao.insert(consulta)
        }
    }
}