package com.martifer.android.videogameskotlin.data.remote

import com.martifer.android.videogameskotlin.data.Game
import com.martifer.android.videogameskotlin.data.GameImage
import com.martifer.android.videogameskotlin.data.model.response.ResponseGameApi
import okhttp3.RequestBody
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST

interface IgdbApiClient {

    /* Search game by name */
    @Headers("Authorization: Bearer ibcomvxc0lkp9iw1ivh9ppielk7nkf", "Client-ID: 2xlviher02b2geatkg1ugfe8tjsyp3", "Content-Type: application/json;charset=utf-8")
    @POST("search")
    //fun getGames(@Body body: RequestBody): Observable<List<Game>>
    suspend fun getGames(@Body body: RequestBody): List<ResponseGameApi>

    /* Search game by id */
    @Headers("Authorization: Bearer ibcomvxc0lkp9iw1ivh9ppielk7nkf", "Client-ID: 2xlviher02b2geatkg1ugfe8tjsyp3", "Content-Type: application/json;charset=utf-8")
    @POST("games")
    suspend fun getGame(@Body body: RequestBody): List<Game>

    /* artworks*/
    @Headers("Authorization: Bearer ibcomvxc0lkp9iw1ivh9ppielk7nkf", "Client-ID: 2xlviher02b2geatkg1ugfe8tjsyp3", "Content-Type: application/json;charset=utf-8")
    @POST("artworks")
    suspend fun getArtworks(@Body body: RequestBody): List<GameImage>

}
