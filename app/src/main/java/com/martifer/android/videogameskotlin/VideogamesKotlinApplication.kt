package com.martifer.android.videogameskotlin

import android.app.Application
import com.martifer.android.videogameskotlin.di.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class VideogamesKotlinApplication : Application() {
    // No need to cancel this scope as it'll be torn down with the process
    val applicationScope = CoroutineScope(SupervisorJob())

    // Using by lazy so the database and the repository are only created when they're needed
    // rather than when the application starts
    //val repository by lazy { ConsultaRepository(database!!.consultaDao(), igdbWebService) }

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger(org.koin.core.logger.Level.NONE)
            androidContext(this@VideogamesKotlinApplication)
            modules(listOf(
                repositoryModule,
                localModule,
                remoteModule,
                mainViewModelModule,
                gameDetailViewModelModule,
                statisticsViewModelModule,
                favoritesViewModelModule,
                useCasesModule,
                dispatchersModule
            ))
        }
    }
}
