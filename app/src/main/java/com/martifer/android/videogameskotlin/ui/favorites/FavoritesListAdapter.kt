package com.martifer.android.videogameskotlin.ui.favorites

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.martifer.android.videogameskotlin.R
import com.martifer.android.videogameskotlin.data.Game

class FavoritesListAdapter :
    ListAdapter<Game, FavoritesListAdapter.FavoritesViewHolder>(CONSULTES_COMPARATOR) {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): FavoritesViewHolder {
        return FavoritesViewHolder.create(parent)
    }

    override fun onBindViewHolder(holder: FavoritesViewHolder, position: Int) {
        val current = getItem(position)
        holder.bind(current.name, current.firstReleaseDate)
    }

    class FavoritesViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val nameItemView: TextView = itemView.findViewById(R.id.favorites_name)
        private val ratingItemView: TextView = itemView.findViewById(R.id.favorites_rating)

        fun bind(text: String?, text2: String?) {
            nameItemView.text = text
            ratingItemView.text = text2.toString()
        }

        companion object {
            fun create(parent: ViewGroup): FavoritesViewHolder {
                val view: View = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_favorites_layout, parent, false)
                return FavoritesViewHolder(view)
            }
        }
    }

    companion object {
        private val CONSULTES_COMPARATOR = object : DiffUtil.ItemCallback<Game>() {
            override fun areItemsTheSame(oldItem: Game, newItem: Game): Boolean {
                return oldItem === newItem
            }

            override fun areContentsTheSame(oldItem: Game, newItem: Game): Boolean {
                return oldItem.name == newItem.name
            }
        }
    }

}