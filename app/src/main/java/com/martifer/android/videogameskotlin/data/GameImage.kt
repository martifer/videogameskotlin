package com.martifer.android.videogameskotlin.data

data class GameImage(
    val id: Int,
    val url: String,
)
