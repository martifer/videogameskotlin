package com.martifer.android.videogameskotlin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.navigation.NavigationView

class MainActivity : AppCompatActivity() {

    private lateinit var drawerLayout: DrawerLayout
    private lateinit var appBarConfiguration: AppBarConfiguration
// https://stackoverflow.com/questions/61322639/android-google-search-bar-with-drawer
    override fun onCreate(savedInstanceState: Bundle?) {
        Log.v("MainActivity", "onCreate");
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        setupNavigationDrawer()
        setSupportActionBar(findViewById(R.id.toolbar1))
        val navController: NavController = findNavController(R.id.fragment)
        appBarConfiguration =
                AppBarConfiguration.Builder(R.id.mainFragment, R.id.statisticsFragment)
                    .setOpenableLayout(drawerLayout)
                        .build()
        setupActionBarWithNavController(navController, appBarConfiguration)
        findViewById<NavigationView>(R.id.nav_view)
                .setupWithNavController(navController)

    }

    override fun onSupportNavigateUp(): Boolean {  // s'ha de posar si no no navega.
        return findNavController(R.id.fragment).navigateUp(appBarConfiguration)
                || super.onSupportNavigateUp()
    }

    private fun setupNavigationDrawer() {
        drawerLayout = (findViewById<DrawerLayout>(R.id.drawer_layout))
                .apply {
                    setStatusBarBackground(R.color.black)
                }
    }
}