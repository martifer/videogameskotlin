package com.martifer.android.videogameskotlin.data.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.martifer.android.videogameskotlin.data.Consulta
import kotlinx.coroutines.flow.Flow

@Dao
interface ConsultaDao {

    @Query("SELECT * FROM taula")
    fun getAlphabetizedConsultes(): Flow<List<Consulta>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(word: Consulta)

    @Query("DELETE FROM taula")
    suspend fun deleteAll()

    @Query("SELECT COUNT(*) FROM taula")
    suspend fun getNumConsultes(): Int
}