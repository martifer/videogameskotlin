package com.martifer.android.videogameskotlin.data.repository

import androidx.annotation.WorkerThread
import com.martifer.android.videogameskotlin.data.Consulta
import com.martifer.android.videogameskotlin.data.Game
import com.martifer.android.videogameskotlin.data.GameEntity
import com.martifer.android.videogameskotlin.data.GameImage
import com.martifer.android.videogameskotlin.data.local.ConsultaDao
import com.martifer.android.videogameskotlin.data.local.GameDao
import com.martifer.android.videogameskotlin.data.model.response.toDomainModel
import com.martifer.android.videogameskotlin.data.remote.IgdbApiClient
import com.martifer.android.videogameskotlin.data.remote.IgdbHelper
import com.martifer.android.videogameskotlin.domain.model.response.ResponseGameModel
import com.martifer.android.videogameskotlin.domain.repository.VideogamesRepository
import kotlinx.coroutines.flow.Flow
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody


class VideogamesRepositoryApi(
    private val consultaDao: ConsultaDao,
    private val gameDao: GameDao,
    private val webservice: IgdbApiClient   /// TODO retrofitBuilder
) : VideogamesRepository {

    override suspend fun getGames(name: String): List<ResponseGameModel> {
        return webservice.getGames(IgdbHelper.prepareGetGamesBody(name)).map { it.toDomainModel() }
    }

    override suspend fun getGame(id: String): List<Game> {
        val body = "fields id, firstReleaseDate, name, rating, summary; where id =" + id + ";"
        val requestBody = body.toRequestBody("application/json".toMediaTypeOrNull())
        return webservice.getGame(requestBody)
    }

    override suspend fun getArtworks(gameId: String): List<GameImage> {
        val body = "fields url; where game =" + gameId + ";"
        val requestBody = body.toRequestBody("application/json".toMediaTypeOrNull())
        return webservice.getArtworks(requestBody)

    }


    // By default Room runs suspend queries off the main thread, therefore, we don't need to
    // implement anything else to ensure we're not doing long running database work
    // off the main thread.
    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    override suspend fun insert(consulta: Consulta) {
        consultaDao.insert(consulta)
    }

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    override suspend fun insertGame(gameEntity: GameEntity) {
        gameDao.insert(gameEntity)
    }

    override suspend fun deleteAll() {
        consultaDao.deleteAll()
    }

    override fun getAlphabetizedConsultes(): Flow<List<Consulta>> {
        return consultaDao.getAlphabetizedConsultes()
    }

    override fun getStoredGames(): Flow<List<GameEntity>> {
        return gameDao.getStoredGames()
    }


}