package com.martifer.android.videogameskotlin.data.remote

import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody

class IgdbHelper {
    companion object {
        fun prepareGetGamesBody(name: String): RequestBody {
            val body =
                "fields *; search \"" + name + "\"; limit 50;"      //"fields *; search \"Zelda\"; limit 50;"
            return body.toRequestBody("application/json".toMediaTypeOrNull())
        }
    }
}