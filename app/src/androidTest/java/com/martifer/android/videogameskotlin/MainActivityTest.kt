package com.martifer.android.videogameskotlin


import android.service.autofill.Validators.not
import android.view.View
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.UiController
import androidx.test.espresso.ViewAction
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.action.ViewActions.closeSoftKeyboard
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.AndroidJUnit4
import org.hamcrest.Matcher
import org.hamcrest.Matchers.allOf
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@LargeTest
@RunWith(AndroidJUnit4::class)
class MainActivityTest {

    @Rule
    @JvmField
    var mActivityTestRule = ActivityTestRule(MainActivity::class.java)

    @Test
    fun mainActivityTest() {
        val textView = onView(
            allOf(
                withId(R.id.search), withContentDescription("buscar"),
                withParent(withParent(withId(R.id.toolbar1))),
                isDisplayed()
            )
        )
        textView.check(matches(withText("")))



        val recyclerView = onView(
            allOf(
                withId(R.id.games_recycler),
                withParent(
                    allOf(
                        withId(R.id.main),
                        withParent(withId(R.id.fragment))
                    )
                ),
                isDisplayed()
            )
        )
        recyclerView.check(matches(isDisplayed()))


        onView(withId(R.id.games_recycler)).check(matches(hasChildCount(0)))

        //textView.perform(typeText("mario"), closeSoftKeyboard())

        /*onView(withId(R.id.search))
            .perform(typeText("Mario"), closeSoftKeyboard());
        */

        //onView(isRoot()).perform(waitFor(3000))

        //onView(withId(R.id.games_recycler)).check(matches(hasMinimumChildCount(1)))

    }

    fun waitFor(delay: Long): ViewAction? {
        return object : ViewAction {
            override fun getConstraints(): Matcher<View> = isRoot()
            override fun getDescription(): String = "wait for $delay milliseconds"
            override fun perform(uiController: UiController, v: View?) {
                uiController.loopMainThreadForAtLeast(delay)
            }
        }
    }
}
